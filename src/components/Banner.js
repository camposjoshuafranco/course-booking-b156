import {Row,Col} from'react-bootstrap';


export const Banner = ({bannerData}) => {
    return (
        <Row className="my-5 px-5">
            <Col>
                <h1>{bannerData.title}</h1>
                <p>{bannerData.content}</p>
            </Col>
        </Row>
    )
}