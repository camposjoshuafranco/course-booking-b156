import { useState, useEffect } from 'react';
import { Banner } from './../components/Banner'
import { Container } from 'react-bootstrap';
import { CourseCard } from '../components/CourseCard';
import { Row, Col} from 'react-bootstrap';

const details = {
    title: 'Welcome to the Courses Page',
    content: 'Here you can view the List of the course'
}



export const Catalog = () => {

    const [courseCollection, setCourseCollection] = useState([])

    useEffect(() => {

        //turn res to json format
        fetch('https://morning-savannah-68233.herokuapp.com/courses/active')
        .then(res => res.json())
        .then(convertedData => {
            setCourseCollection(convertedData.map(course => {
            return (
                <Col xs={12} md={6} lg={4} key={course._id}>
                    <CourseCard courseProp={course} />
                </Col>
            )
        }))
        })

    }, [courseCollection]);

    return (
        <>
            <Banner bannerData={details} />
            <Container fluid className="px-5"> 
                <Row>           
                    {courseCollection}
                </Row>
            </Container>
        </>
        
    )
}
