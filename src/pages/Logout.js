import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

export const LogoutPage = () => {
    const { setUser, unsetUser } = useContext(UserContext);

    unsetUser();

    useEffect(() => {
        setUser({
            id:null,
            isAdmin: null
        })
    },[setUser])

    return (
            <Navigate to="/login" replace={true} />
    )
}