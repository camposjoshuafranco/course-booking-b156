import { useState, useEffect, useContext } from'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import { Banner } from './../components/Banner'
import { Container, Form, Button } from "react-bootstrap";
import Swal from 'sweetalert2';


const details = {
    title: 'Welcome to the LoginPage',
    content: 'Enter details here'
}

export const LoginPage = () => {

    const { user,setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    let addressSign = email.search('@');
    let dns = email.search('.com');

    const [isActive, setIsActive] = useState(false);
    const [isValid, setIsValid] = useState(false);


    useEffect(() => {
        if (dns !== -1 && addressSign !== -1) {
            setIsValid(true)
            if (password !== '') {
                setIsActive(true)
            } else {
                setIsActive(false)
            }
        } else {
            setIsValid(false)
            setIsActive(false)
        }
    },[email, password, addressSign, dns, setIsValid])
    


    const loginUser = async (event) => {
        event.preventDefault();

        fetch('https://morning-savannah-68233.herokuapp.com/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(res => res.json())
        .then(dataJson => {
            let token = dataJson.accessToken;

            if (typeof token !== 'undefined'){

                localStorage.setItem('accessToken',token)
                
                fetch('https://whispering-spire-20350.herokuapp.com/users/details', {
				   headers: {
				      Authorization: `Bearer ${token}`
				   }
				})
				.then(res => res.json())
				.then(convertedData => {
				   
				   if (typeof convertedData._id !== "undefined") {

                       setUser({
                           id: convertedData._id, 
                           isAdmin: convertedData.isAdmin
                        });
                        
                        Swal.fire({
                          icon: 'success',
                          title: 'Login Successful',
                          text: 'Welcome!'
                       })	
				   } else {
				      //if the condition above is not met
				      setUser({
				        id: null, 
				        isAdmin: null
				     });
				   }
				}); 

                
            }else {
                Swal.fire({
                    icon: 'error',
                    title: 'Check Credentials',
                    text: 'Contact Adman if problem persist'
                })
            }
        })

    };

    return (
        user.id
        ?
            <Navigate to="/courses" replace={true} />
        :
        <>
            <Banner bannerData={details} />
            <Container className="mb-5 w-75 border rounded">
            <h1 className="text-center">Login</h1>
            <Form className="mb-5" onSubmit={ e => loginUser(e)}>
                <Form.Group>
                    <Form.Label>Email</Form.Label>
                    <Form.Control 
                        type="email"
                        placeholder="Enter Email"
                        required
                        value={email}
                        onChange={event => {setEmail(event.target.value)} }
                    />
                    {isValid ? <h6 className="text-success">Email is Valid!</h6> : <h6 className="text-secondary">Email is not Valid</h6>}
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password"
                        placeholder="Enter Paassword"
                        required
                        value={password}
                        onChange={e => {setPassword(e.target.value)}}
                    />
                </Form.Group>
                {
                    isActive ?
                    <Button 
                        className="btn-info btn-main btn-block"
                        type="submit"
                    >
                        Log in
                    </Button>
                        :
                        <Button 
                        className="btn-secondary btn-block"
                        disabled
                    >
                        Log in
                    </Button>
                }
                </Form>
            </Container>
        </>
        
    )
}
