import { Banner } from './../components/Banner'
import { Highlights } from './../components/Highlights'

const details = {
    title: 'Welcome to the Home Page',
    content: 'Where we strive for excellence, Ad majorem dei gloriam'
}

export const Home = () => {
    return (
        <div>
            <Banner bannerData={details} />
            <Highlights />
        </div>
        
    )
}
